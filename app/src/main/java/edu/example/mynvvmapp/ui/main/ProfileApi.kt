package edu.example.mynvvmapp.ui.main

interface ProfileApi {
    suspend fun load():Profile
    suspend fun logout()
}