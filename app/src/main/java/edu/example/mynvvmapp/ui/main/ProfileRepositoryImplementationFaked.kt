package edu.example.mynvvmapp.ui.main

import kotlinx.coroutines.delay

class ProfileRepositoryImplementationFaked : ProfileViewModel.Repository {
    override suspend fun loadProfile(): Profile {
        delay(1000L)
        return Profile(
            id = 1,
            name = "User",
            email = "example@email.com"
        )
    }

    override suspend fun logout() {
        delay(1000L)
        throw Throwable("it's a faked method")
    }

}